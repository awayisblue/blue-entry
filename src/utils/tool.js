const crypto = require('crypto')
const qs = require('qs')
// 从object里过滤出需要的字段
module.exports.filterPropertys = (obj, propetys = []) => {
  let result = {}
  propetys.forEach((key) => {
    obj[key] !== undefined && (result[key] = obj[key])
  })
  return result
}
// 字典序排序，并检验query, 注：queryString加密的时候要加上secret，然后进行字典序排序，再md5得到token, 传输的时候，要加上token，但secret注意不要传输
module.exports.checkToken = (queryObj, secret) => {
  let token = queryObj.token
  delete queryObj.token
  queryObj.secret = secret
  let queryString = qs.stringify(queryObj)
  let sorted = queryString.split('&').sort((a, b) => {
    if (a > b) return 1
    if (a < b) return -1
    return 0
  })
  return md5(sorted.join('&')) === token
}
function md5 (str) {
  return crypto.createHash('md5').update(str).digest('hex')
}
