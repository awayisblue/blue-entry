let errCode = 2000
module.exports = {
  commonStatus: {
    '200': {
      statusCode: 200,
      data: {
        code: 0,
        msg: 'OK'
      }
    },
    '301': {
      statusCode: 301,
      data: {
        code: 301,
        msg: 'Moved Permanently'
      }
    },
    '302': {
      statusCode: 302,
      data: {
        code: 302,
        msg: 'Redirecting'
      }
    },
    '404': {
      statusCode: 404,
      data: {
        code: 404,
        msg: 'Not Found'
      }
    },
    '403': {
      statusCode: 403,
      data: {
        code: 403,
        msg: 'Forbidden'
      }
    }
  },
  OK: {
    statusCode: 200,
    data: {
      code: 0,
      msg: 'OK'
    }
  },
  PARAM_ERROR: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: '参数错误'
    }
  },
  SERVER_ERROR: {
    statusCode: 500,
    data: {
      code: errCode++,
      msg: '服务器错误'
    }
  },
  UNAUTHORIZED: {
    statusCode: 403,
    data: {
      code: errCode++,
      msg: '未登录'
    }
  },
  NOT_FOUND: {
    statusCode: 404,
    data: {
      code: errCode++,
      msg: '404 not found'
    }
  },
  ENTRY_EXISTS: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: '入口已存在'
    }
  },
  ENTRY_NOT_EXISTS: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: '入口不存在'
    }
  },
  TOKEN_ERROR: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: 'token校验失败'
    }
  },
  VOID_SECRET_PARAMS: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: '参数不要带secret'
    }
  },
  LOGIN_STALE: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: '登录链接已失效'
    }
  }
}
