const config = require('config')
function compose (...rest) {
  return rest.join(':')
}
// 综合管理redis的key
let appName = config.appName || '__entry'
module.exports.pageConfig = function (pageName) {
  return compose(appName, '__pageConfig', pageName)
}
