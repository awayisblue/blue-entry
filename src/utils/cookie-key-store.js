const config = require('config')
let appName = config.appName || '__entry'
module.exports.session = function () {
  return `${appName}_entry_sid`
}
module.exports.entryName = function () {
  return `${appName}_entry_name`
}
