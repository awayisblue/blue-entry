const error = require('../utils/error-store')
const config = require('config')
module.exports = function () {
  return async (ctx, next) => {
    let token = ctx.get('x-mount-point-token')
    if (token === config.mountPointToken) return next()
    throw error.UNAUTHORIZED
  }
}
