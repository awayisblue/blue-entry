const cookieKeyStore = require('../utils/cookie-key-store')
const redisKeyStore = require('../utils/redis-key-store')
const redisDB = require('../clients/redis')
const jwt = require('jsonwebtoken')
const path = require('path')
const config = require('config')
module.exports = function () {
  return async (ctx, next) => {
    let pageName = ctx.params.pageName
    // 目前使用了pageContent, passport, jwtSecret, ignorePattern 3个字段
    let pageConfig = await redisDB.hgetall(redisKeyStore.pageConfig(pageName))
    ctx.state.pageConfig = pageConfig
    if (!pageConfig.passport || !pageConfig.jwtSecret) return next()
    if (pageConfig.ignorePattern) {
      let regex = new RegExp(pageConfig.ignorePattern)
      // 从pageName之后开始match, 比如 /entry/root/abc, 会被 /entry/root 所split, 得到 /abc
      let testString = ctx.path.split(path.join(config.server.prefix, pageName))[1]
      if (regex.test(testString)) return next()
    }
    let sid = ctx.cookies.get(cookieKeyStore.session())
    try {
      let decoded = jwt.verify(sid, pageConfig.jwtSecret)
      if (decoded) return next()
    } catch (err) {
      // 当前url
      let currentUrl = ctx.href
      let loginUrl = path.join(ctx.request.origin, config.server.prefix, '/sso/login', pageName)
      let returnUrl = `${loginUrl}?return_url=${currentUrl}`
      let passport = pageConfig.passport.split('#')[0]
      let symbol = passport.indexOf('?') > -1 ? '&' : '?'
      // 这里使用跳转的方式来做，具体登录逻辑做到passport链接所对应的服务里去
      ctx.redirect(`${passport}${symbol}return_url=${encodeURIComponent(returnUrl)}`)
    }
  }
}
