const Router = require('koa-router')
const router = new Router()
const redisDB = require('../../clients/redis')
const redisKeyStore = require('../../utils/redis-key-store')
const cookieKeyStore = require('../../utils/cookie-key-store')
const error = require('../../utils/error-store')
const tool = require('../../utils/tool')
const jwt = require('jsonwebtoken')
const config = require('config')
const path = require('path')
const moment = require('moment')
// 单点登录
router.get('/login/:pageName', async function (ctx, next) {
  let pageName = ctx.params.pageName
  if (ctx.query.secret) throw error.VOID_SECRET_PARAMS
  let secret = await redisDB.hget(redisKeyStore.pageConfig(pageName), 'jwtSecret')
  let validToken = tool.checkToken(JSON.parse(JSON.stringify(ctx.query)), secret)
  if (!validToken) throw error.TOKEN_ERROR
  let timeStamp = ctx.query.timeStamp
  let reqTime = moment.unix(timeStamp)
  // 限定偏差10s以内可用
  if (!reqTime.isValid() || Math.abs(moment().diff(reqTime)) > 10000) {
    throw error.LOGIN_STALE
  }
  let returnUrl = ctx.query.return_url
  delete ctx.query.token
  delete ctx.query.return_url
  delete ctx.query.timeStamp
  // 把单点登录带过来的，除了token, timeStamp及return_url参数之外的参数都加了jwt数据里面
  let sid = jwt.sign(ctx.query, secret, {expiresIn: '1d'})
  let entryUrl = path.join(ctx.request.origin, config.server.prefix, pageName)
  ctx.cookies.set(cookieKeyStore.session(), sid, {
    maxAge: 23 * 60 * 60 * 1000,
    path: entryUrl, // 不同的entry使用不同的cookie
    httpOnly: false
  })
  ctx.redirect(returnUrl || entryUrl)
})
// 获取系统时间, 用于同步时间
router.get('/time', async function (ctx, next) {
  ctx.body = {
    time: moment().format('X')
  }
})
module.exports = router
