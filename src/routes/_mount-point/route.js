const Router = require('koa-router')
const router = new Router()
const validate = require('validate.js')
const error = require('../../utils/error-store')
const tool = require('../../utils/tool')
const redisDB = require('../../clients/redis')
const redisKeyStore = require('../../utils/redis-key-store')
// 提供配置相关的api，供前端请求配置
router.get('/menus', async function (ctx, next) {
  ctx.body = {
    list: [
      {
        name: '入口配置',
        desc: '',
        path: '/entrys', // 相对于 _mount-point的path
        methods: ['get', 'post'], // 支持get, post ,put, delete
        pagination: false, // 是否支持分页
        pageKey: 'page', // 页码
        amountKey: 'amount', // 单页数据量
        fields: { // fields代表请求得到的数据所拥有的字段，可以做为post新增数据的依据，也可以做为修改单个数据的依据
          pageName: {
            presence: {allowEmpty: false},
            fieldInfo: {
              name: '页面名称',
              desc: '通过页面名称组装请求url',
              noEdit: true // 不支持修改
            }
          },
          pageContent: {
            presence: {allowEmpty: false},
            fieldInfo: {
              name: '页面内容',
              desc: '填入html的源码'
            }
          },
          passport: {
            presence: false,
            fieldInfo: {
              name: '跳转登录链接',
              desc: '假如设置了该字段，则需要用户有登录才能获取页面内容，假如没有登录，会跳转到该链接进行登录，完成逻辑后，要请求login接口进行单点登录，让系统有登录信息'
            }
          },
          jwtSecret: {
            presence: false,
            fieldInfo: {
              name: '进行单点登录的加密密钥，也是jwt session的decode密钥',
              desc: ''
            }
          },
          ignorePattern: {
            presence: false,
            fieldInfo: {
              name: '无需登录的pattern',
              desc: '当开启了登录才能进页面时，这里的字符表示不需要登录的链接格式（会转换成正则表达式进行路径匹配），系统匹配的是入口之后的path, 如/entry/pageName/test, 拿来匹配的是/test'
            }
          }
        }
      }
    ]
  }
})
// 查所有entrys
router.get('/entrys', async function (ctx, next) {
  let pattern = redisKeyStore.pageConfig('*')
  let keys = await redisDB.keys(pattern)
  let result = keys.map((key) => {
    let pageName = key.split(':').pop()
    return {
      name: pageName,
      desc: '',
      path: `/entrys/${pageName}`,
      methods: ['get', 'put', 'delete']
    }
  })
  ctx.body = {
    list: result
  }
})
// 新增entry
router.post('/entrys', async function (ctx, next) {
  const constrains = {
    pageName: {
      presence: {allowEmpty: false}
    },
    pageContent: {
      presence: {allowEmpty: false}
    },
    passport: {
      presence: false
    },
    jwtSecret: {
      presence: false
    },
    ignorePattern: {
      presence: false
    }
  }
  let postBody = ctx.request.body
  if (validate(postBody, constrains)) {
    throw error.PARAM_ERROR
  }
  let data = tool.filterPropertys(postBody, Object.getOwnPropertyNames(constrains))
  data['create_at'] = (new Date()).getTime()
  let redisKey = redisKeyStore.pageConfig(data.pageName)
  let exists = await redisDB.exists(redisKey)
  if (exists) throw error.ENTRY_EXISTS
  await redisDB.hmset(redisKey, data)
  ctx.body = {}
})
// 查某个entry
router.get('/entrys/:name', async function (ctx, next) {
  let pageName = ctx.params.name
  let pageConfig = await redisDB.hgetall(redisKeyStore.pageConfig(pageName))
  ctx.body = {
    data: pageConfig
  }
})
// 修改某个entry
router.put('/entrys/:name', async function (ctx, next) {
  const constrains = {
    pageContent: {
      presence: {allowEmpty: false}
    },
    passport: {
      presence: false
    },
    jwtSecret: {
      presence: false
    },
    ignorePattern: {
      presence: false
    }
  }
  let postBody = ctx.request.body
  if (validate(postBody, constrains)) {
    throw error.PARAM_ERROR
  }
  let data = tool.filterPropertys(postBody, Object.getOwnPropertyNames(constrains))
  let pageName = ctx.params.name
  let redisKey = redisKeyStore.pageConfig(pageName)
  let exists = await redisDB.exists(redisKey)
  if (!exists) throw error.ENTRY_NOT_EXISTS
  await redisDB.hmset(redisKey, data)
  ctx.body = {}
})
// 删除某个entry
router.del('/entrys/:name', async function (ctx, next) {
  let pageName = ctx.params.name
  let redisKey = redisKeyStore.pageConfig(pageName)
  let exists = await redisDB.exists(redisKey)
  if (!exists) throw error.ENTRY_NOT_EXISTS
  await redisDB.del(redisKey)
  ctx.body = {}
})
module.exports = router
