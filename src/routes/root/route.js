const Router = require('koa-router')
const router = new Router()
const passport = require('../../middlewares/passport')
// 支持单页面
router.get(['/:pageName', '/:pageName/*'], passport(), async function (ctx, next) {
  let pageContent = ctx.state.pageConfig.pageContent
  if (!pageContent) return next()
  ctx.body = pageContent
})
module.exports = router
